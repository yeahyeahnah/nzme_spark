
(function(compId){var _=null,y=true,n=false,x1='6.0.0',x8='rgba(255,255,255,0.00)',x2='5.0.0',lf='left',g='image',zx='scaleX',x3='6.0.0.400',e9='${_45}',h='height',xc='rgba(0,0,0,1)',zy='scaleY',e10='${slacker}',w='width',tp='top',x4='rgba(255,255,255,1.00)',x5='rgba(0,0,0,0)',o='opacity',e11='${Ellipse}',i='none';var g6='45.png',g7='slacker.png';var im='images/',aud='media/',vid='media/',js='js/',fonts={},opts={'gAudioPreloadPreference':'auto','gVideoPreloadPreference':'auto'},resources=[],scripts=[],symbols={"stage":{v:x1,mv:x2,b:x3,stf:i,cg:i,rI:n,cn:{dom:[{id:'Ellipse',t:'ellipse',r:['8px','8px','243px','243px','auto','auto'],br:["50%","50%","50%","50%"],o:'0',f:[x4],s:[0,xc,i],tf:[[],[],[],['0.01','0.01']]},{id:'_45',t:g,r:['-1px','1px','260px','260px','auto','auto'],o:'0',f:[x5,im+g6,'0px','0px'],tf:[[],[],[],['0.01','0.01']]},{id:'slacker',t:g,r:['32px','32px','198px','198px','auto','auto'],o:'0',f:[x5,im+g7,'0px','0px'],tf:[[],[],[],['0.01','0.01']]}],style:{'${Stage}':{isStage:true,r:['null','null','260px','260px','auto','auto'],overflow:'hidden',f:[x8]}}},tt:{d:2095,a:n,data:[["eid66",o,1173,922,"easeOutCirc",e9,'0','1'],["eid54",h,1000,0,"linear",e10,'198px','198px'],["eid35",zy,321,1250,"easeOutSine",e11,'0.01','1'],["eid64",h,2095,0,"easeOutCirc",e9,'260px','260px'],["eid33",o,321,1250,"easeOutSine",e11,'0','1'],["eid62",tp,2095,0,"easeOutCirc",e9,'1px','1px'],["eid53",w,1000,0,"linear",e10,'198px','198px'],["eid68",zx,1173,922,"easeOutCirc",e9,'0.01','1'],["eid24",h,321,0,"easeOutElastic",e11,'243px','243px'],["eid29",h,1571,0,"easeOutElastic",e11,'243px','243px'],["eid58",zy,0,1000,"linear",e10,'0.01','1'],["eid61",lf,2095,0,"easeOutCirc",e9,'-1px','-1px'],["eid52",tp,1000,0,"linear",e10,'32px','32px'],["eid63",w,2095,0,"easeOutCirc",e9,'260px','260px'],["eid70",zy,1173,922,"easeOutCirc",e9,'0.01','1'],["eid23",w,321,0,"easeOutElastic",e11,'243px','243px'],["eid28",w,1571,0,"easeOutElastic",e11,'243px','243px'],["eid51",lf,1000,0,"linear",e10,'32px','32px'],["eid34",zx,321,1250,"easeOutSine",e11,'0.01','1'],["eid56",zx,0,1000,"linear",e10,'0.01','1'],["eid60",o,0,1000,"linear",e10,'0','1'],["eid21",lf,321,0,"easeOutElastic",e11,'8px','8px'],["eid26",lf,1571,0,"easeOutElastic",e11,'8px','8px'],["eid22",tp,321,0,"easeOutElastic",e11,'8px','8px'],["eid27",tp,1571,0,"easeOutElastic",e11,'8px','8px']]}}};AdobeEdge.registerCompositionDefn(compId,symbols,fonts,scripts,resources,opts);})("EDGE-16638794");
(function($,Edge,compId){var Composition=Edge.Composition,Symbol=Edge.Symbol;Edge.registerEventBinding(compId,function($){
//Edge symbol: 'stage'
(function(symbolName){Symbol.bindSymbolAction(compId,symbolName,"creationComplete",function(sym,e){
    sym.play();
    function isScrolledIntoView(elem) {
        var docViewTop = $(window).scrollTop();
        var docViewBottom = docViewTop + $(window).height();
        var elemTop = elem.offset().top;
        var elemBottom = elemTop + elem.height();
        return ((elemBottom >= docViewTop) && (elemTop <= docViewBottom)
          && (elemBottom <= docViewBottom) &&  (elemTop >= docViewTop) );
    }
    var element = sym.getSymbolElement();
    if(isScrolledIntoView(element)) {
        sym.play(0)
    } else {
        $(window).on("scroll", function(e) {
            if(isScrolledIntoView(element)) {
                sym.play(0);
                $(window).off("scroll");
            }
        });
    }});
})("stage");
//Edge symbol end:'stage'
})})(AdobeEdge.$,AdobeEdge,"EDGE-16638794");